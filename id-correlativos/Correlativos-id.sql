use master
go
--create database AutonomusSQL
go
use AutonomusSQL
go
if(object_id('CorrelativoIdentity')is not null)
begin drop table CorrelativoIdentity end 
go
create table CorrelativoIdentity(
	Id int identity not null
	, Nombre varchar(50) null
	, primary key(Id) -- Importante llave primaria.
)
insert into CorrelativoIdentity values('Prueba y error 543')
									,('Prueba y error 133')
									,('Prueba y error 542')
									,('Prueba y error 985')
go
--select * from CorrelativoIdentity
----------------------------------------------------------------------------------
if(object_id('CorrelativoCeroIzq')is not null)
begin drop table CorrelativoCeroIzq end 
go
create table CorrelativoCeroIzq(
	Id varchar(5) not null
	, Nombre varchar(50) null
	, primary key(Id) -- Importante llave primaria.
)
insert into CorrelativoCeroIzq values('00001','Prueba y error 543')
									,('00002','Prueba y error 133')
									,('00003','Prueba y error 542')
									,('00004','Prueba y error 985')
go
----------------------------------------------------------------------------------
if(object_id('CorrelativoLetraCeroIzq')is not null)
begin drop table CorrelativoLetraCeroIzq end 
go
create table CorrelativoLetraCeroIzq(
	Id varchar(6) not null
	, Nombre varchar(50) null
	, check(Id like'X[0-9][0-9][0-9][0-9][0-9]')-- Importante validar que solo admita este formato
	, primary key(Id) -- Importante llave primaria.
)
insert into CorrelativoLetraCeroIzq values('X00001','Prueba y error 543')
									,('X00002','Prueba y error 133')
									,('X00003','Prueba y error 542')
									,('X00004','Prueba y error 985')
go
----------------------------------------------------------------------------------
if(object_id('CorrelativoAñoMesCeroIzq')is not null)
begin drop table CorrelativoAñoMesCeroIzq end 
go
create table CorrelativoAñoMesCeroIzq(
	Id varchar(10) not null
	, Nombre varchar(50) null
	, check(Id like'[0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9]')
	-- Importante validar que solo admita este formato (AÑOMES-TRES.DIGITOS)
	, primary key(Id) -- Importante llave primaria.
)
/*
insert into CorrelativoAñoMesCeroIzq values('202103-001','Prueba y error 543')
									,('202103-002','Prueba y error 133')
									,('202103-003','Prueba y error 542')
									,('202103-004','Prueba y error 985')

insert into CorrelativoAñoMesCeroIzq values('202104-001','Prueba y error 543')
									,('202104-002','Prueba y error 133')
									,('202104-003','Prueba y error 542')
									,('202104-004','Prueba y error 985')
*/
-------------------------------------------------------------------------------------
-- select * From CorrelativoIdentity -- No es necesario crear el ID

------------------------------------------------------------------------
	select * from CorrelativoCeroIzq
--(1)-- Obtener el valor máximo : Convertir a entero 
	select MAX(Id) from CorrelativoCeroIzq
--(2)-- Validacion NULL  y Agregar +1
	select isnull(MAX(Id),0)+1 from CorrelativoCeroIzq
--(3)-- Concatenar con ceros ..  valor:convertir a varchar
	select '000000'+convert(varchar,isnull(MAX(Id),0)+1) from CorrelativoCeroIzq
--(4)-- Obtener los ultimos digitos para el ID	
	declare @nuevo_id varchar(5)=(select right('000000'+convert(varchar,isnull(MAX(Id),0)+1),5) from CorrelativoCeroIzq);	
	insert into CorrelativoCeroIzq values(@nuevo_id
	,'Prueba y error 543')

------------------------------------------------------------------------
	select * from CorrelativoLetraCeroIzq
--(1)-- Obtener el valor máximo
	select Max(Id) from CorrelativoLetraCeroIzq
--(2)-- Separar letra y Numero
	select Max(right(Id,5)) from CorrelativoLetraCeroIzq
--(3)-- Validacion NULL  y Agregar +1
	select isnull(Max(right(Id,5)),0)+1 from CorrelativoLetraCeroIzq
--(4)-- Concatenar con ceros ..  valor:convertir a varchar
	select '000000000'+convert(varchar,isnull(Max(right(Id,5)),0)+1) from CorrelativoLetraCeroIzq
--(5)-- Obtener los ultimos digitos para el ID	
	select right('000000000'+convert(varchar,isnull(Max(right(Id,5)),0)+1),5) from CorrelativoLetraCeroIzq
--(6)-- Concatenar la Primera Letra y el Valor
	select 'X'+right('000000000'+convert(varchar,isnull(Max(right(Id,5)),0)+1),5) from CorrelativoLetraCeroIzq

	declare @nuevo_idX varchar(6)=(select 'X'+right('000000000'+convert(varchar,isnull(Max(right(Id,5)),0)+1),5) from CorrelativoLetraCeroIzq);
	insert into CorrelativoLetraCeroIzq values(@nuevo_idX,'Prueba y error 543')

------------------------------------------------------------------------
	select * from CorrelativoAñoMesCeroIzq
--(1)-- Obtener el valor máximo
	select max(Id) from CorrelativoAñoMesCeroIzq
--(2)-- Separar letra y Numero
	select max(right(Id,3)) from CorrelativoAñoMesCeroIzq
--(3)-- Validacion NULL  y Agregar +1
	select isnull(max(right(Id,3)),0)+1 from CorrelativoAñoMesCeroIzq
--(4)-- Concatenar con ceros ..  valor:convertir a varchar
	select '00000'+convert(varchar,isnull(max(right(Id,3)),0)+1) from CorrelativoAñoMesCeroIzq
--(5)-- Extraer los ultimos digitos del correlativo
	select right('00000'+convert(varchar,isnull(max(right(Id,3)),0)+1),3) from CorrelativoAñoMesCeroIzq
--(6)-- Generar Año y mes en formato(01,02..12)
	--Generar año
		select year(getdate())
	--Generar Mes con formato '00' dos digitos
		select right('00'+convert(varchar,month(getdate())),2)
	-- Concatenar Año Mes y Separador("-")
		select convert(varchar,year(getdate()))+right('00'+convert(varchar,month(getdate())),2)+'-'
--(6)-- Concatenar valores finales
		select convert(varchar,year(getdate()))+right('00'+convert(varchar,month(getdate())),2)+'-'
			+ right('00000'+convert(varchar,isnull(max(right(Id,3)),0)+1),3)
		 from CorrelativoAñoMesCeroIzq
--(7)-- Reiniciar el contador o correlativo si cambia de año-mes
	-- 1era forma: extraer valores
		select substring(Id,1,6) from CorrelativoAñoMesCeroIzq
	-- 2da forma: extraer valores
		select left(Id,6) from CorrelativoAñoMesCeroIzq
	-- Validacion final
		select convert(varchar,year(getdate()))+right('00'+convert(varchar,month(getdate())),2)+'-'
			+ right('00000'+convert(varchar,isnull(max(right(Id,3)),0)+1),3)
		 from CorrelativoAñoMesCeroIzq
		 where left(Id,6)=(select convert(varchar,year(getdate()))+right('00'+convert(varchar,month(getdate())),2))
	
	declare @año varchar(4)=(year(getdate()));
	declare @mes varchar(2)=(right('00'+convert(varchar,month(getdate())),2));
	declare @nuievo_id2 varchar(10)=(
		select @año+@mes+'-'
			+ right('00000'+convert(varchar,isnull(max(right(Id,3)),0)+1),3)
		 from CorrelativoAñoMesCeroIzq
		 where left(Id,6)=(@año+@mes)
	)
	
	insert into CorrelativoAñoMesCeroIzq values(@nuievo_id2,'Prueba y error 543')
